var express = require('express');
var server = express();
var path = require('path');

var data =['a','b','c','d'];

server.get('/:page', function (req, res, next){
    var page = parseInt(req.params.page) - 1;
    page = page < 0 ? 0 : page;
    var start = page * 2;
    var end = start + 2;
    res.json({
        status: 'success',
        date: data.slice(start, end)
    });
})
server.listen(3000, function(){
    console.log('ok');
})