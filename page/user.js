var express = require('express');
var router = express.Router();

function middleware(req, res, next) {
  res.render('login.ejs');
}

router.use(middleware);


router.get('/login', function(req,res,next){
    res.render('header.ejs');
   
})

router.get('/profile', function(req,res,next){
    res.send('PROFILE');
})


module.exports = router;